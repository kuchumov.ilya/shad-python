import csv
from enum import Enum
from collections import defaultdict
from scipy.optimize import minimize
import numpy as np, numpy.random


class Components(Enum):
    B6 = 'Vit_B6_(mg)'
    C = 'Vit_C_(mg)'
    FE = 'Iron_(mg)'
    CA = 'Calcium_(mg)'
    MG = 'Magnesium_(mg)'
    P = 'Phosphorus_(mg)'
    KCAL = 'Energ_Kcal'


def get_default_coefficients():
    return {comp.value: 1 for comp in Components}


def get_contained_ratio(product, component):
    product_volume = 0
    for comp in product:
        if comp in ['NDB_No', 'GmWt_1', 'GmWt_Desc1', 'GmWt_Desc1', 'GmWt_Desc1', 'Refuse_Pct']:
            continue
        try:
            product_volume += float(product[comp])
        except ValueError:
            continue
    try:
        component_volume = float(product[component])
    except ValueError:
        return 0
    return component_volume / product_volume


def get_profit(product, coefficients):
    return sum([get_contained_ratio(product, comp.value) * coefficients[comp.value] for comp in Components])


def print_top_products(products, coefficients):
    sorted_products = sorted(products, key=lambda p: get_profit(p, coefficients), reverse=True)
    for index, product in enumerate(sorted_products[:10]):
        print('{}: {} has profit {}'.format(index, product['Shrt_Desc'], get_profit(product, coefficients)))


def get_day_needs():
    return {
        Components.B6.value: 1.5,
        Components.C.value: 100,
        Components.FE.value: 10,
        Components.CA.value: 1200,
        Components.MG.value: 400,
        Components.P.value: 2,
        Components.KCAL.value: 2500
    }


def get_components_percentage(product, product_volume):
    day_needs = get_day_needs()
    components_percentage = dict()
    for comp in Components:
        try:
            p = float(product[comp.value]) / day_needs[comp.value] * product_volume
        except ValueError:
            p = 0
        components_percentage[comp.value] = p
    return components_percentage


def get_total_components_percentage(products, products_volume):
    components_percentage = defaultdict(float)
    for index, product in enumerate(products):
        current_percentage = get_components_percentage(product, products_volume[index])
        for comp in current_percentage:
            components_percentage[comp] += current_percentage[comp]
    return components_percentage


def build_f(products):
    def f(products_volume):
        ret = -min(get_total_components_percentage(products, products_volume).values())
        s = sum(products_volume)
        ret += 1e9 * (1 - s) ** 2
        # print(ret)
        return ret

    return f


def print_best_products(best_ans, top_products):
    print('Minimum ratio: {}. You should eat:'.format(-best_ans[0]))
    for i in range(len(best_ans)-2):
        id = best_ans[2+i]
        print('{} with ratio {}'.format(top_products[id]['Shrt_Desc'], best_ans[1][i]))


def main():
    with open('ABBREV.csv') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',', quotechar='"')
        products = list(reader)

    # print_top_products(products, get_default_coefficients())

    sorted_products = sorted(products, key=lambda p: get_profit(p, get_default_coefficients()), reverse=True)
    n = 30
    top_products = sorted_products[:n]

    best_ans = [0]
    for i1 in range(n):
        for i2 in range(i1 + 1, n):
            for i3 in range(i2 + 1, n):
                for i4 in range(i3 + 1, n):
                    for i5 in range(i4 + 1, n):
                        current_products = [top_products[i1],
                                            top_products[i2],
                                            top_products[i3],
                                            top_products[i4],
                                            top_products[i5]]
                        f = build_f(current_products)
                        x0 = np.random.dirichlet(np.ones(len(current_products)), size=1)
                        bounds = list((0, 1) for x in range(len(current_products)))
                        res = minimize(f, x0, bounds=bounds)
                        if res.fun < best_ans[0]:
                            best_ans = [res.fun, res.x, i1, i2, i3, i4, i5]
                            print_best_products(best_ans, top_products)
    print(best_ans)

if __name__ == '__main__':
    main()
