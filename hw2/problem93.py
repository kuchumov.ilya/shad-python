"""Problem 93 from Project Euler

Answer: 1258 with prefix length 51

It's just recursion. Nothing interesting :(
"""

import numpy as np


def rec(values, possible_sums, functions):
    if len(values) == 1:
        possible_sums.add(values[0])
        return
    for i, a in enumerate(values):
        for j, b in enumerate(values):
            if i == j:
                continue
            values_without_operands = np.delete(values, [i, j]).tolist()
            for f in functions:
                try:
                    rec(values_without_operands + [f(a, b)],
                        possible_sums,
                        functions)
                except ZeroDivisionError:
                    pass


def get_prefix_length(start_values, eq_function):
    possible_sums = set()
    functions = ((lambda x, y: x + y),
                 (lambda x, y: x - y),
                 (lambda x, y: x * y),
                 (lambda x, y: x / y))
    rec(start_values, possible_sums, functions)
    ans = 0
    while any(eq_function(x, ans + 1) for x in possible_sums):
        ans += 1
    return ans


def main():
    best_l = 0
    for a in range(10):
        for b in range(a + 1, 10):
            for c in range(b + 1, 10):
                for d in range(c + 1, 10):
                    l = get_prefix_length([a, b, c, d],
                                          lambda x, y: abs(x - y) < 1e-9)
                    if l > best_l:
                        best_l = l
                        print('{}{}{}{} {}'.format(a, b, c, d, l))


if __name__ == '__main__':
    main()
