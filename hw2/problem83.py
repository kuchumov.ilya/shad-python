"""Problem 83 from Project Euler

Answer for the input file: 425185

This solution uses Bellman-Ford algorithm. Quite simple but a bit slow solution.
More detailed description about algorithm:
https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm.

"""


def read_matrix(path):
    with open(path, encoding='utf8') as file:
        matrix = []
        for line in file.read().split('\n'):
            matrix.append(list(map(int, line.split(','))))
        return matrix


def relax(matrix, min_sum):
    rows = len(matrix)
    columns = len(matrix[0])
    for i in range(rows):
        for j in range(columns):
            for dx, dy in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                ni = i + dx
                nj = j + dy
                if 0 <= ni < rows and 0 <= nj < columns:
                    min_sum[ni][nj] = min(min_sum[ni][nj],
                                          min_sum[i][j] + matrix[ni][nj])


def solve(matrix):
    rows = len(matrix)
    columns = len(matrix[0])
    min_sum = [[float("inf") for i in range(columns)] for i in range(rows)]
    min_sum[0][0] = matrix[0][0]
    for it in range(rows * columns):
        print('it = {}'.format(it))
        relax(matrix, min_sum)
    return min_sum[rows - 1][columns - 1]


def solve_for_path(path):
    return solve(read_matrix(path))


def main():
    print(solve_for_path('big-matrix.txt'))

if __name__ == '__main__':
    main()
