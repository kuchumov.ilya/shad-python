"""Problem 67 from Project Euler

Answer for the input file: 7273

Lets define tr(x, y) as a value in the cell (x, y) of the given triangle. Path
from the cell (x, y) can go to (x + 1, y) or to (x + 1, y + 1) if corresponding
cell exists.

Lets define f(x, y) as the maximum sum between all correct paths from (0, 0) to
(x, y). Lets assume that f(x, y) = 0 if corresponding cell doesn't exists.
It's true that f(x, y) = max(f(x - 1, y - 1), f(x - 1, y)) + tr(x, y). So we can
use dynamic programming approach to solve this problem.

"""

import copy
import itertools


def read_triangle(path):
    with open(path, encoding='utf8') as file:
        triangle = []
        for line in file.read().split('\n'):
            triangle.append(list(map(int, line.split())))
        return triangle


def solve(triangle):
    triangle = copy.deepcopy(triangle)
    for i in range(len(triangle)):
        for j in range(len(triangle[i])):
            prev_values = []
            if i - 1 >= 0 and j < len(triangle[i - 1]):
                prev_values.append(triangle[i - 1][j])
            if i - 1 >= 0 and j - 1 >= 0:
                prev_values.append(triangle[i - 1][j - 1])
            if len(prev_values):
                triangle[i][j] += max(prev_values)
    return max(itertools.chain.from_iterable(triangle))


def solve_for_path(path):
    return solve(read_triangle(path))


def main():
    print(solve_for_path('big-triangle.txt'))

if __name__ == '__main__':
    main()
