import urllib.request
import re
import queue
import threading
import glob
import tkinter
import random


'''
This application downloads topics from main page stackoverflow, downloads
these pages and stores them into pages folder.

After application creates a small window and changes words in it every 1000 ms.

User can change speed of showing.
'''


def generate_urls():
    opener = urllib.request.FancyURLopener({})
    f = opener.open("http://stackoverflow.com/")
    s = f.read().decode('utf-8')
    urls = []
    for line in re.findall(r'<a href=".*?" class="question-hyperlink">', s):
        a = len('<a href="')
        b = len('" class="question-hyperlink">')
        url = 'http://stackoverflow.com' + line[a:-b]
        urls.append(url)
    return urls


def worker(q):
    while True:
        item = q.get()
        if item is None:
            break
        opener = urllib.request.FancyURLopener({})
        f = opener.open("http://stackoverflow.com/")
        s = f.read().decode('utf-8')
        name = item[item.rfind('/') + 1:]
        print(name)
        with open('pages/' + name, 'w+') as file:
            file.write(s)
        q.task_done()


def download(num_of_threads):
    urls = generate_urls()
    q = queue.Queue()
    for url in urls:
        q.put(url)
    threads = []
    for i in range(num_of_threads):
        t = threading.Thread(target=worker, args=(q,))
        t.start()
        threads.append(t)
    q.join()
    for i in range(num_of_threads):
        q.put(None)
    for t in threads:
        t.join()


def read_topics():
    return [file[len('pages/'):].replace('-', ' ') for file in
            glob.glob("pages/*")]


class App:
    def __init__(self, topics):
        self.root = tkinter.Tk()
        self.root.title("lazy reading")
        self.root.minsize(width=1000, height=30)
        self.timeout = 1000
        self.label = tkinter.Label(text="are you ready?")
        self.label.config(font=("Courier", 20))
        self.label.pack()
        tkinter.Button(self.root, text="start", command=self.doit).pack()
        tkinter.Button(self.root, text="faster", command=self.faster).pack()
        tkinter.Button(self.root, text="slower", command=self.slower).pack()
        self.topics = topics
        self.root.mainloop()

    def faster(self):
        self.timeout = max(1, self.timeout - 100)

    def slower(self):
        self.timeout += 100

    def doit(self):
        text = random.choice(self.topics)
        self.label.configure(text=text)
        self.root.after(self.timeout, self.doit)


def main():
    # download(num_of_threads=2)
    topics = read_topics()
    App(topics)


main()
