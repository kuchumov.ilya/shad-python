from predictor import Predictor
import text_parser
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--paths', nargs='+', default=['train.txt'])
    parser.add_argument('--tuple_length', type=int, default=3)
    parser.add_argument('--text_length', type=int, default=50)
    return parser.parse_args()


def build_pred_from_files(paths, max_tuple_len):
    pred = Predictor()
    for path in paths:
        text = text_parser.parse(path)
        pred.learn(text, max_tuple_len)
    return pred


def build_sentence(pred, max_tuple_len, max_sentence_len):
    current_tuple = ()
    sentence = []
    while len(sentence) < max_sentence_len:
        next_word = pred.predict(current_tuple)
        if next_word is None:
            break
        sentence.append(next_word)
        current_tuple += (next_word,)
        if len(current_tuple) > max_tuple_len:
            current_tuple = current_tuple[1:]
    return ' '.join(sentence).capitalize().replace(' ,', ',') + '.'


def main():
    args = parse_args()
    pred = build_pred_from_files(args.paths, args.tuple_length)
    text = build_sentence(pred, args.tuple_length, args.text_length)
    print(text)


if __name__ == '__main__':
    main()