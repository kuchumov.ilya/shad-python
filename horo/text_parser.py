import re


def parse(path):
    with open(path, 'r', encoding='utf-8') as file:
        text = file.read()
    return [word.lower() for word in re.findall('\w+|[,]', text) if
            len(word) != 0]
