from collections import defaultdict
import random


class Predictor:
    def __init__(self):
        self.tuples_extensions = defaultdict(list)

    def learn(self, sentence, max_tuple_length):
        real_max_tuple_length = min(max_tuple_length, len(sentence) - 1)
        self.tuples_extensions[()].extend(sentence)
        for tuple_length in range(1, real_max_tuple_length + 1):
            for l in range(len(sentence) - tuple_length):
                tuple_from = tuple(sentence[l:l+tuple_length])
                str_to = sentence[l+tuple_length]
                self.tuples_extensions[tuple_from].append(str_to)

    def predict(self, last_tuple):
        if last_tuple not in self.tuples_extensions:
            return None
        return random.choice(self.tuples_extensions[last_tuple])
