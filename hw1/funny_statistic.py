import argparse
import itertools
import re


def parse_args():
    parser = argparse.ArgumentParser(
        description='Funny statistic interactor.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--path',
                        help='path to text which you are interested in',
                        default='ВиМ.txt')
    return parser.parse_args()


def print_strings(sentences):
    print('=' * 80)
    for index, sentence in enumerate(sentences):
        print('{}) {}'.format(index, sentence))
    print('=' * 80)


def task0(sentences):
    print_strings([sentence
                   for sentence in sentences
                   if u'тся' in sentence and u'ться' in sentence])


def task1(sentences):
    print_strings([sentence
                   for sentence in sentences
                   if u'чтобы' in sentence])


def task2(sentences):
    print_strings([sentence
                   for sentence in sentences
                   if u'что бы' in sentence])


def task3(sentences):
    some_introduction_words = [u'Наконец', u'Однако', u'Значит']

    def is_good(sentence):
        return any([word in sentence for word in some_introduction_words])

    print_strings([sentence for sentence in sentences if is_good(sentence)])


def task4(sentences):
    words = itertools.chain.from_iterable([re.findall(r'\w+', sentence)
                                           for sentence in sentences])
    unique_words = list(set(words))
    sorted_words = sorted(unique_words, key=len, reverse=True)
    print_strings(sorted_words[:10])


def task5(sentences):
    sorted_sentences = sorted(sentences, key=len, reverse=True)
    print_strings(sorted_sentences[:5])


def task6(sentences):
    sorted_sentences = sorted(sentences,
                              key=lambda s: s.count(u'р'),
                              reverse=True)
    print_strings(sorted_sentences[:5])


def task7(sentences):
    words = itertools.chain.from_iterable([re.findall(r'\w+', sentence)
                                           for sentence in sentences])

    def is_palindrome(word):
        return word == ''.join(reversed(word))

    palindromes = [word for word in words if is_palindrome(word)]
    unique_palindromes = list(set(palindromes))
    sorted_palindromes = sorted(unique_palindromes, key=len, reverse=True)
    print_strings(sorted_palindromes[:10])


def main():
    args = parse_args()
    path = args.path
    print('Preparing...')
    with open(path, encoding='utf-8') as file:
        text = file.read()
    sentences = text.split('.')
    all_tasks = [
        ['Print all sentences which contains "ться" and "тся"'
         'at the same time.', task0],
        ['Print all sentences which contains "чтобы".', task1],
        ['Print all sentences which contains "что бы".', task2],
        ['Print all sentences which contains introduction words.', task3],
        ['Print top 10 longest words.', task4],
        ['Print top 5 longest sentences.', task5],
        ['Print top 5 sentences with the most "р" frequency.', task6],
        ['Print top 10 longest palindromes.', task7],
    ]
    try:
        while True:
            print('I can:')
            for index, task in enumerate(all_tasks):
                print('{}: {}'.format(index, task[0]))
            print('-- Choose the task id for me (integer number from [0-{}]).'
                  'Use ctrl+C to exit.'
                  .format(len(all_tasks)))
            index = int(input())
            task = all_tasks[index][1]
            task(sentences)
    except KeyboardInterrupt:
        print('\nBye!')

if __name__ == '__main__':
    main()
